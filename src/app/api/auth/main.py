from fastapi import FastAPI, HTTPException, Depends, Request
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel
from fastapi import APIRouter
import bcrypt


# from app.api.users.crud import create_user as crud_create_user
from app.db import users, db

router = APIRouter()

# in production you can use Settings management
# from pydantic to get secret key from .env

class User(BaseModel):
    login: str
    password: str

# provide a method to create access tokens. The create_access_token()
# function is used to actually generate the token to use authorization
# later in endpoint protected
@router.post('/login')
async def login(user_data: User, Authorize: AuthJWT = Depends()):
    query = users.users.select().where(user_data.login == users.users.c.login)
    user = await db.database.fetch_one(query=query)

    if not user:
        raise HTTPException(status_code=401, detail="Bad username or password")

    req = dict(user.items())
    id = req.get('user_id')
    login = req.get('login')
    email = req.get('email')
    password = str(req.get('password_hash')).encode('utf-8')

    if not bcrypt.hashpw(user_data.password.encode('utf-8'), password) == password:
        raise HTTPException(status_code=401, detail="Bad username or password")

    # subject identifier for who this token is for example id or username from database
    payload = {
        "login": login,
        "email": email
    }

    access_token = Authorize.create_access_token(user_claims=payload, subject=id)

    return {"access_token": access_token}