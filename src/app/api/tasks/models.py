from pydantic import BaseModel, Field
from datetime import datetime as dt
from pytz import timezone as tz

# create table public.task (
#   task_id serial not null primary key,
#   task_nm text,
#   formulation text,
#   answer text,
#   topic text,
#   complexity text, --In future: enum (1,2,3,4,5)
#   operator_array text, --may be not text, something else
#   created_dttm timestamp,
#   updated_dttm timestamp,
#   deleted_dttm timestamp,
#   lesson_id int references public.lesson(lesson_id)
# );

class TaskCreateDto(BaseModel):
    title: str = Field(..., min_length=3)
    formulation: str = Field(..., min_length=3)
    answer: str = Field()
    complexity: str = Field(..., min_length=3)
    topic: str = Field(..., min_length=3)
    operator_array: str = Field(..., min_length=3)
    lesson_id: int = Field()

class TaskCreateResponse(BaseModel):
    task_id: int

class TaskDto(TaskCreateDto):
    task_id: int