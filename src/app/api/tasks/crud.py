from app.db.tasks import tasks
from app.db.db import database
from datetime import datetime as dt
from app.api.tasks.models import TaskCreateDto

# tasks = Table(
#     "tasks",
#     metadata,
#     Column("task_id", Integer, primary_key=True),
#     Column("task_nm", String),
#     Column("formulation", String),
#     Column("answer", String),
#     Column("topic", String),
#     Column("complexity", String),
#     Column("operator_array", String),
#     Column("lesson_id", Integer), # FIXME: change to user_id for foreign key
#     schema='public',
# )


async def create_task(task_data: TaskCreateDto):
    created_dttm = dt.now().strftime("%Y-%m-%d %H:%M")

    task = tasks.insert().values(
        task_nm=task_data.title,
        formulation=task_data.formulation,
        answer=task_data.answer,
        topic=task_data.topic,
        complexity=task_data.complexity,
        operator_array=task_data.operator_array,
        lesson_id=task_data.lesson_id
    )

    task_id = await database.execute(task)

    return task_id

async def get_task(task_id):
    query = tasks.select().where(task_id == tasks.c.task_id)
    task = await database.fetch_one(query)
    return task

async def get_all_tasks(search: str = '', lesson_ids: list[int] = []):
    query = tasks.select().filter(tasks.c.task_nm.ilike(f"%{search}%"), tasks.c.lesson_id.in_(lesson_ids) if(len(lesson_ids) > 0) else True)

    task_list = await database.fetch_all(query)
    return task_list