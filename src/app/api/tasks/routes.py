from app.api.tasks import crud
from app.api.tasks.models import TaskCreateResponse, TaskCreateDto, TaskDto
from fastapi import APIRouter, HTTPException, Path
from datetime import datetime as dt
from fastapi import FastAPI, HTTPException, Depends, Request, Query
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel
from typing import List 

router = APIRouter()

def o2d(obj):
    return dict(obj.items())

@router.post("", response_model=TaskCreateResponse, status_code=200)
async def create_task(payload: TaskCreateDto):
    task_id = await crud.create_task(payload)
    created_dttm = dt.now()

    response_object = {
        "task_id": task_id,
    }

    return response_object

@router.get("/{task_id}", response_model=TaskDto)
async def read_task(task_id: int = Path(..., gt=0),):
    task = await crud.get_task(task_id)

    if not task:
        raise HTTPException(status_code=404, detail="Task not found")
    
    c = o2d(task)

    response = {
        'task_id': c["task_id"],
        'title': c["task_nm"],
        'formulation': c["formulation"],
        'answer': c["answer"],
        'complexity': c["complexity"],
        'operator_array': c["operator_array"],
        'topic': c["topic"],
        'lesson_id': c["lesson_id"],
    }

    return response

@router.get("/list/", response_model=List[TaskDto])
async def read_all_tasks(search: str = Query(''), lesson_ids: List[int] = Query([])):
    tasks = await crud.get_all_tasks(search, lesson_ids)

    res = []
    for task in tasks:
        c = o2d(task)
        response = {
            'task_id': c["task_id"],
            'title': c["task_nm"],
            'formulation': c["formulation"],
            'answer': c["answer"],
            'complexity': c["complexity"],
            'operator_array': c["operator_array"],
            'topic': c["topic"],
            'lesson_id': c["lesson_id"],
        }

        res.append(response)
    return res