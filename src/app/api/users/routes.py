from app.api.users import crud
from app.api.users.models import UserCreateDto, UserCreateResponse, UserDto, UserUpdateDto, UserUpdateResponse
from fastapi import APIRouter, HTTPException, Path
from typing import List 
from datetime import datetime as dt
from fastapi import FastAPI, HTTPException, Depends, Request
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel

router = APIRouter()

@router.post("", response_model=UserCreateResponse, status_code=200)
async def create_user(payload: UserCreateDto):
    user_id = await crud.create_user(payload)
    created_dttm = dt.now()

    response_object = {
        "user_id": user_id,
        "login": payload.login,
        # "password_hash": payload.password_hash,
        # "first_nm": payload.first_nm,
        # "last_nm": payload.last_nm,
        # "birth_dt": payload.birth_dt,
        "email": payload.email,
        # "is_student": payload.is_student,
        # "rating": payload.rating,
        # "created_dttm": created_dttm,
    }

    return response_object
    
@router.get("/{user_id}", response_model=UserDto)
async def read_user(user_id: int = Path(..., gt=0),):
    user = await crud.get_user(user_id)

    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user

@router.get("/list/", response_model=List[UserDto])
async def read_all_users():
    return await crud.get_all_users()

@router.put("/{user_id}", response_model=UserUpdateResponse)
async def update_user(user_id: int = Path(..., gt=0), payload: UserUpdateDto = {"email": "string"}):
    user = await crud.get_user(user_id)

    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    await crud.update_user(user_id, payload)

    newUser = dict(user)
    newUser.update({'email': payload.email})

    return newUser

@router.delete("/{user_id}", response_model=UserDto)
async def delete_user(user_id: int = Path(..., gt=0)):
    user = await crud.get_user(user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    await crud.delete_user(user_id)
    return user