from pydantic import BaseModel, Field
from datetime import datetime as dt
from pytz import timezone as tz

# Base = declarative_base()

# class User(Base):
#     __tablename__ = 'users'
    
#     user_id = Column(Integer, primary_key=True, autoincrement=True)
#     login = Column(String)
#     password_hash = Column(String)
#     first_nm = Column(String)
#     last_nm = Column(String)
#     birth_dt = Column(Date)
#     email = Column(String)
#     is_student = Column(Boolean)
#     rating = Column(Numeric)
#     created_dttm = Column(DateTime)
#     updated_dttm = Column(DateTime)
#     deleted_dttm = Column(DateTime)

class UserCreateDto(BaseModel):
    login: str = Field(..., min_length=3, max_length=50) #additional validation for the inputs 
    email: str = Field(..., min_length=3, max_length=50)
    password: str = Field(..., min_length=3, max_length=50)

class UserDto(BaseModel):
    user_id: int = Field()
    login: str = Field()
    email: str = Field()

class UserUpdateDto(BaseModel):
    email: str = Field()
    
class UserUpdateResponse(BaseModel):
    user_id: int = Field()
    login: str = Field()
    email: str = Field()

class UserCreateResponse(BaseModel):
    user_id: int