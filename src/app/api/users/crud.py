from app.db import users, db
from datetime import datetime as dt
from app.api.users.models import UserDto
import bcrypt

async def create_user(user_data: UserDto):
    created_dttm = dt.now().strftime("%Y-%m-%d %H:%M")
    salt = bcrypt.gensalt()
    hashed_password = bcrypt.hashpw(user_data.password.encode('utf-8'), salt)

    user = users.users.insert().values(
        login=user_data.login,
        password_hash=hashed_password.decode('utf8'),
        # first_nm=user_data.first_nm,
        # last_nm=user_data.last_nm,
        # birth_dt=user_data.birth_dt,
        email=user_data.email,
        # is_student=user_data.is_student,
        # rating=user_data.rating,
        # created_dttm=created_dttm
    )

    user_id = await db.database.execute(user)

    return user_id

async def get_user(user_id):
    query = users.users.select().where(user_id == users.users.c.user_id)
    user = await db.database.fetch_one(query)
    return user

async def get_all_users():
    query = users.users.select()
    users_list = await db.database.fetch_all(query)
    return users_list

async def update_user(user_id, user_data):
    updated_dttm = dt.now().strftime("%Y-%m-%d %H:%M")

    user = (
        users.users.update()
        .where(user_id == users.users.c.user_id)
        .values(
            email=user_data.email,
        )
        .returning(users.users.c.user_id)
    )

    await db.database.execute(user)
    return {"user_id": user_id}

async def delete_user(user_id):
    query = users.users.delete().where(user_id == users.users.c.user_id)
    await db.database.execute(query)
    return {"result": "User deleted successfully"}