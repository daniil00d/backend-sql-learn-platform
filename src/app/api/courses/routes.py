from app.api.courses import crud
from app.api.courses.models import CourseCreateResponse, CourseCreateDto, CourseDto
from fastapi import APIRouter, HTTPException, Path
from datetime import datetime as dt
from fastapi import FastAPI, HTTPException, Depends, Request
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel
from typing import List 

router = APIRouter()

def o2d(obj):
    return dict(obj.items())

@router.post("", response_model=CourseCreateResponse, status_code=200)
async def create_course(payload: CourseCreateDto):
    course_id = await crud.create_course(payload)
    created_dttm = dt.now()

    response_object = {
        "course_id": course_id,
    }

    return response_object

@router.get("/{course_id}")
async def read_course(course_id: int = Path(..., gt=0),):
    course = await crud.get_course(course_id)

    if not course:
        raise HTTPException(status_code=404, detail="User not found")
    
    c = o2d(course)
    response = {
        'course_id': c["course_id"],
        'title': c["course_nm"],
        'description': c["description"],
        'topic': c["topic"],
    }

    return response

@router.get("/list/", response_model=List[CourseDto])
async def read_all_courses(search: str = ''):
    courses = await crud.get_all_courses(search)

    res = []
    for course in courses:
        c = o2d(course)
        response = {
            'course_id': c["course_id"],
            'title': c["course_nm"],
            'description': c["description"],
            'topic': c["topic"],
        }

        res.append(response)
    return res