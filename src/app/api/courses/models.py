from pydantic import BaseModel, Field
from datetime import datetime as dt
from pytz import timezone as tz

# create table public.course (
#   course_id serial not null primary key,
#   course_nm text,
#   topic text,
#   description text,
#   created_dttm timestamp,
#   updated_dttm timestamp,
#   deleted_dttm timestamp,
#   user_id int references public.user(user_id)
# );
class CourseCreateDto(BaseModel):
    title: str = Field(..., min_length=3)
    topic: str = Field(..., min_length=3)
    description: str = Field(..., min_length=3)

class CourseCreateResponse(BaseModel):
    course_id: int

class CourseDto(CourseCreateDto):
    course_id: int