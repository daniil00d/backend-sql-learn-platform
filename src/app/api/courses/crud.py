from app.db.courses import courses
from app.db.db import database
from datetime import datetime as dt
from app.api.courses.models import CourseCreateDto

# metadata,
# Column("course_id", Integer, primary_key=True),
# Column("course_nm", String(50)),
# Column("topic", String(50)),
# Column("description", String(50)),
# Column("user_id", ForeignKey("users.user_id")),
# schema='public',
async def create_course(course_data: CourseCreateDto):
    created_dttm = dt.now().strftime("%Y-%m-%d %H:%M")


    course = courses.insert().values(
        course_nm=course_data.title,
        topic=course_data.topic,
        description=course_data.description,
        user_id=1, # FIXME
    )

    course_id = await database.execute(course)

    return course_id

async def get_course(course_id):
    query = courses.select().where(course_id == courses.c.course_id)
    course = await database.fetch_one(query)
    return course

async def get_all_courses(search: str = ''):
    query = courses.select().filter(courses.c.course_nm.ilike(f"%{search}%"))

    course_list = await database.fetch_all(query)
    return course_list