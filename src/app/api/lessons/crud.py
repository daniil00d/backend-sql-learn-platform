from app.db.lessons import lessons
from app.db.db import database
from datetime import datetime as dt
from app.api.lessons.models import LessonCreateDto

# lessons = Table(
#     "lessons",
#     metadata,
#     Column("lesson_id", Integer, primary_key=True),
#     Column("lesson_nm", String),
#     Column("topic", String),
#     Column("material", String),
#     Column("course_id", Integer), # FIXME: change to user_id for foreign key
#     schema='public',
# )

async def create_lesson(lesson_data: LessonCreateDto):
    created_dttm = dt.now().strftime("%Y-%m-%d %H:%M")


    lesson = lessons.insert().values(
        lesson_nm=lesson_data.title,
        topic=lesson_data.topic,
        material=lesson_data.material,
        course_id=1, # FIXME
    )

    lesson_id = await database.execute(lesson)

    return lesson_id

async def get_lesson(lesson_id):
    query = lessons.select().where(lesson_id == lessons.c.lesson_id)
    lesson = await database.fetch_one(query)
    return lesson

async def get_all_lessons(search: str = '', course_ids: list[int] = []):
    query = lessons.select().filter(lessons.c.lesson_nm.like(f"%{search}%"), lessons.c.course_id.in_(course_ids) if(len(course_ids) > 0) else True)

    lesson_list = await database.fetch_all(query)
    return lesson_list