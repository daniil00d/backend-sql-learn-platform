from app.api.lessons import crud
from app.api.lessons.models import LessonCreateResponse, LessonCreateDto, LessonDto
from fastapi import APIRouter, HTTPException, Path
from datetime import datetime as dt
from fastapi import FastAPI, HTTPException, Depends, Request, Query
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic import BaseModel
from typing import List 

router = APIRouter()

def o2d(obj):
    return dict(obj.items())

@router.post("", response_model=LessonCreateResponse, status_code=200)
async def create_lesson(payload: LessonCreateDto):
    lesson_id = await crud.create_lesson(payload)
    created_dttm = dt.now()

    response_object = {
        "lesson_id": lesson_id,
    }

    return response_object

@router.get("/{lesson_id}", response_model=LessonDto)
async def read_lesson(lesson_id: int = Path(..., gt=0),):
    lesson = await crud.get_lesson(lesson_id)

    if not lesson:
        raise HTTPException(status_code=404, detail="User not found")
    
    c = o2d(lesson)
    response = {
        'lesson_id': c["lesson_id"],
        'title': c["lesson_nm"],
        'material': c["material"],
        'topic': c["topic"],
        'course_id': c["course_id"],
    }

    return response

@router.get("/list/", response_model=List[LessonDto])
async def read_all_lessons(search: str = '', course_ids: List[int] = Query([])):
    lessons = await crud.get_all_lessons(search, course_ids)
    

    res = []
    for lesson in lessons:
        c = o2d(lesson)
        response = {
            'lesson_id': c["lesson_id"],
            'title': c["lesson_nm"],
            'material': c["material"],
            'topic': c["topic"],
            'course_id': c["course_id"],
        }

        res.append(response)
    
    return res