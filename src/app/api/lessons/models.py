from pydantic import BaseModel, Field
from datetime import datetime as dt
from pytz import timezone as tz

# create table public.lesson (
#   lesson_id serial not null primary key ,
#   lesson_nm text,
#   topic text,
#   material text, --markdown probably
#   course_id int references public.course(course_id),
#   created_dttm timestamp,
#   updated_dttm timestamp,
#   deleted_dttm timestamp
# );

class LessonCreateDto(BaseModel):
    title: str = Field(..., min_length=3)
    topic: str = Field(..., min_length=3)
    material: str = Field()
    course_id: int = Field()

class LessonCreateResponse(BaseModel):
    lesson_id: int

class LessonDto(LessonCreateDto):
    lesson_id: int