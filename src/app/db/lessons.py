import os

from sqlalchemy import (Column, Integer, String, Table)
from datetime import datetime as dt
from app.db.db import metadata

# FIXME: replace with course fields
# create table public.lesson (
#   lesson_id serial not null primary key ,
#   lesson_nm text,
#   topic text,
#   material text, --markdown probably
#   course_id int references public.course(course_id),
#   created_dttm timestamp,
#   updated_dttm timestamp,
#   deleted_dttm timestamp
# );

lessons = Table(
    "lessons",
    metadata,
    Column("lesson_id", Integer, primary_key=True),
    Column("lesson_nm", String),
    Column("topic", String),
    Column("material", String),
    Column("course_id", Integer), # FIXME: change to user_id for foreign key
    schema='public',
)
