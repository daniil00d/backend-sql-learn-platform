import os

from sqlalchemy import (Column, Integer, String, Table)
from datetime import datetime as dt
from app.db.db import metadata

# FIXME: replace with course fields
# create table public.task (
#   task_id serial not null primary key,
#   task_nm text,
#   formulation text,
#   answer text,
#   topic text,
#   complexity text, --In future: enum (1,2,3,4,5)
#   operator_array text, --may be not text, something else
#   created_dttm timestamp,
#   updated_dttm timestamp,
#   deleted_dttm timestamp,
#   lesson_id int references public.lesson(lesson_id)
# );

tasks = Table(
    "tasks",
    metadata,
    Column("task_id", Integer, primary_key=True),
    Column("task_nm", String),
    Column("formulation", String),
    Column("answer", String),
    Column("topic", String),
    Column("complexity", String),
    Column("operator_array", String),
    Column("lesson_id", Integer), # FIXME: change to user_id for foreign key
    schema='public',
)
