import os

from sqlalchemy import (Column, Integer, String, Table)
from datetime import datetime as dt
from app.db.db import metadata

# FIXME: replace with course fields
# create table public.course (
#   course_id serial not null primary key,
#   course_nm text,
#   topic text,
#   description text,
#   created_dttm timestamp,
#   updated_dttm timestamp,
#   deleted_dttm timestamp,
#   user_id int references public.user(user_id)
# );
# );
courses = Table(
    "courses",
    metadata,
    Column("course_id", Integer, primary_key=True),
    Column("course_nm", String),
    Column("topic", String),
    Column("description", String),
    Column("user_id", Integer), # FIXME: change to user_id for foreign key
    schema='public',
)
