import os

from sqlalchemy import (Column, Integer, String, Table)
from dotenv import load_dotenv
from databases import Database
from datetime import datetime as dt
from pytz import timezone as tz

from app.db.db import metadata

# FIXME: replace with user fields
# create table public.user (
#   user_id serial not null primary key ,
#   login text,
#   password_hash text,
#   first_nm text,
#   last_nm text,
#   birth_dt date,
#   email text,
#   is_student bool,
#   rating numeric,
#   created_dttm timestamp,
#   updated_dttm timestamp,
#   deleted_dttm timestamp
#   --level ? xz, may be we don't need it
# );
users = Table(
    "users",
    metadata,
    Column("user_id", Integer, primary_key=True),
    Column("login", String(50)),
    Column("password_hash", String),
    Column("email", String(50)),
    schema='public',
)
