DROP DATABASE IF EXISTS sqlp_dev;    

CREATE DATABASE sqlp_dev;    

\c sqlp_dev;        

create table public.user (
  user_id serial not null primary key ,
  login text,
  password_hash text,
  first_nm text,
  last_nm text,
  birth_dt date,
  email text,
  is_student bool,
  rating numeric,
  created_dttm timestamp,
  updated_dttm timestamp,
  deleted_dttm timestamp
  --level ? xz, may be we don't need it
);

create table public.course (
  course_id serial not null primary key,
  course_nm text,
  topic text,
  description text,
  created_dttm timestamp,
  updated_dttm timestamp,
  deleted_dttm timestamp,
  user_id int references public.user(user_id)
);

create table public.lesson (
  lesson_id serial not null primary key ,
  lesson_nm text,
  topic text,
  material text, --markdown probably
  course_id int references public.course(course_id),
  created_dttm timestamp,
  updated_dttm timestamp,
  deleted_dttm timestamp
);


create table public.task (
  task_id serial not null primary key,
  task_nm text,
  formulation text,
  answer text,
  topic text,
  complexity text, --In future: enum (1,2,3,4,5)
  operator_array text, --may be not text, something else
  created_dttm timestamp,
  updated_dttm timestamp,
  deleted_dttm timestamp,
  lesson_id int references public.lesson(lesson_id)
);

create table public.progress(
  task_id int references public.task(task_id),
  user_id int references public.user(user_id),
  passed_flg bool,
  last_solution text, --May be we need solutions history?
  created_dttm timestamp,
  updated_dttm timestamp,
  deleted_dttm timestamp,
  PRIMARY KEY (task_id, user_id)
);